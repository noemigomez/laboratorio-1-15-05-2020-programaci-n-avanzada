#!/usr/bin/env python3
 # -*- coding:utf-8 -*-
import random

class Motor():

    def __init__(self, cilindrada):
        self.cilindrada = random.choice(cilindrada)

class Estanque():

    def __init__(self, motor):
        """
            En el caso de cilindrada 1.2, el consumo es de 1 L cada 20 Km
            a velocidad máxima (120 km/h), por lo que a la velocidad que sea
            se gasta 1 L a los 1/6 km (v = 120 km/h, gasto 1/6 * v = 20 km)
            De la misma manera, la relación con cilandrada 1.6, son 1 L cada
            14 km, a velocidad máxima. 1 L a 7/60 km (7/60 * v = 14 km)
        """
        self.combustible = 32
        if motor.cilindrada == 1.2:
            self.consumo = 1 / 6
        elif motor.cilindrada == 1.6:
            self.consumo = 7 / 60

    def cambio(self, perdida):
        self.combustible = self.combustible - perdida

class Rueda():

    def __init__(self):
        self.desgaste = 0
        self.cantidad_desgaste = random.randrange(1, 11)

    def desgaste_avance(self, v):
        # v: número de movimiento
        self.desgaste = self.cantidad_desgaste * v

class Velocimetro():

    def __init__(self):
        self.velocidad = random.randrange(10, 121)
