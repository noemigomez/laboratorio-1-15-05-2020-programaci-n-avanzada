#!/usr/bin/env python3
 # -*- coding:utf-8 -*-
from ObjetosAuto import Motor
from ObjetosAuto import Estanque
from ObjetosAuto import Rueda
from ObjetosAuto import Velocimetro
import time
import os
import random

class Automovil():

    def __init__ (self):
        ruedas = []
        self.motor = Motor([1.2, 1.6])
        self.__estanque = Estanque(self.motor)
        # crea una lista con las 4 ruedas
        for i in range(4):
            ruedas.append(Rueda())
        self.__ruedas = ruedas
        self.__velocimetro = Velocimetro()
        # marcha es el estado del auto False: apagado
        self.marcha = False
        # forma de auto
        self.pres = "o°--o°"

    def get_estanque(self):
        return self.__estanque

    def set_estanque(self, perdida):
        # cambia en el objeto estanque
        self.__estanque.cambio(perdida)

    def get_velocimetro(self):
        return self.__velocimetro

    def set_velocimetro(self):
        self.__velocimetro = self.get_velocimetro().inicial()

    def get_ruedas(self):
        return self.__ruedas

    def set_ruedas(self):
        # veces que la rueda ha recorrido (cuando se cambia, es 0)
        j = 0
        for i in self.get_ruedas():
            # las desgasta
            i = i.desgaste_avance(self.v[j])
            j += 1

    def on_off(self):
        # si está apagado, lo enciende
        if self.marcha is False:
            self.marcha = True
            # al encenderlo, pierde el 1% del combustible
            perdida = self.get_estanque().combustible * 0.01
            self.set_estanque(perdida)

        else:
            self.marcha = False


    def estado(self, a, t):
        time.sleep(0.5)
        os.system('clear')
        # autito
        print(a + self.pres)
        print("Tiempo transcurrido (segundos): {0}".format(t))
        print("Ha recorrido {0} km".format(round(self.d, 4)))
        print("Se mueve a {0} km/h".format(self.get_velocimetro().velocidad))
        # sobrepasaba los 79 caracteres
        com = round(self.get_estanque().combustible, 2)
        print("{0} L de combustible".format(com))
        print(("El estado de las ruedas son: "))
        # número de rueda para imprimir
        r = 0
        for i in self.get_ruedas():
            r += 1
            print("Desgaste rueda {0}: {1}%".format(r, i.desgaste))

    def reparar_ruedas(self):
        # se reparan las ruedas desgastadas
        for i in self.a_cambiar:
            print("La rueda {0} se gastó más de un 90%.".format(i + 1))
            # rueda nueva
            self.get_ruedas()[i] = Rueda()
            self.v[i] = 0
            print("Acaba de ser reparada.")

    def revisar(self):
        # la(s) rueda(s) a cambiar
        self.a_cambiar = []
        for i in range(4):
            if self.get_ruedas()[i].desgaste > 90:
                self.a_cambiar.append(i)
        # si no se necesita cambiar ninguna
        if len(self.a_cambiar) != 0:
            return True
        else:
            return False

    def cambio_estanque(self, porkm):
        # si la distancia es mayor porkm, se gasta combustible
        if self.tempd >= porkm:
            # puede ser 2 veces porkm, en ese caso se gastan 2 L, no 1
            self.set_estanque(self.tempd / porkm)
            self.tempd = 0

    def mover(self):
        try:
            # veces de avance de cada rueda
            self.v = [0, 0, 0, 0]
            # tiempo inicial 0
            t = 0
            # tiempo que dura el movimiento
            tempt = random.randrange(1, 11)
            # distancia inicial
            self.tempd = 0
            # para imprimir auto
            a = ""
            # tiene un consumo de L cada ciertos kilometros, segun cilindrada
            porkm = self.get_estanque().consumo * self.get_velocimetro().velocidad

            while self.get_estanque().combustible > 0:
                # dividir 3600 por segundos, velocidad en km por hora
                self.d = self.get_velocimetro().velocidad * (t / 3600)
                self.tempd += self.d
                a += " "
                self.set_ruedas()
                self.estado(a, t)
                self.cambio_estanque(porkm)
                t += tempt
                # cambio en veces de avance de las ruedas
                for j in range(4):
                    self.v[j] += 1
                rev = self.revisar()
                if rev is True:
                    # se apaga el auto y se cambia rueda
                    self.on_off()
                    self.reparar_ruedas()
                    volver = str(input(("¿Desea volver a encender el auto? (S: si): ")))
                    if volver.upper() == "S":
                        continue
                    else:
                        break
                else:
                    continue
            self.d = self.get_velocimetro().velocidad * (t / 3600)
        except ValueError:
            pass
