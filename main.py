#!/usr/bin/env python3
 # -*- coding:utf-8 -*-
from Automovil import Automovil
import time

def recorrido(auto):
    if auto.get_estanque().combustible <= 0:
        print("El auto se quedó sin combustible.")
        print("Recorrió {0} km.".format(round(auto.d, 3)))

def encendido(auto):
    if auto.marcha is True:
        print("Auto encendido.")
    else:
        print("Auto detenido.")


if __name__ == "__main__":
    try:
        auto = Automovil()
        #encendido(auto)
        _onoff = str(input("Ingrese S para encender el auto: "))
        if _onoff.upper() == "S":
            auto.on_off()
            encendido(auto)
            move = str(input("Presione A para comenzar a avanzar: "))
            if move.upper() == "A":
                auto.mover()
                encendido(auto)
            recorrido(auto)
    except ValueError:
        encendido(auto)
